//
// Created by tfryett on 4/24/21.
//

#ifndef SILHOUETTE_LAYER_HXX
#define SILHOUETTE_LAYER_HXX

namespace sil {

  constexpr uint8_t MAX_LAYER_NUMBER = 63;
  constexpr uint8_t DEFAULT_LAYER_NUMBER = 1;
  static_assert(DEFAULT_LAYER_NUMBER < MAX_LAYER_NUMBER,
                "DEFAULT_LAYER_NUMBER must be smaller than MAX_LAYER_NUMBER");

  class LayerProperty {
      private:
      uint8_t layer;

      public:
      LayerProperty(uint8_t my_layer) {
        if (!setLayer(my_layer)) {
          throw std::invalid_argument("Invalid layer number. "
                                      "Layers must be between 0 and " +
                                      std::to_string(MAX_LAYER_NUMBER) +
                                      ". Recieved: " + std::to_string(my_layer) +
                                      ".");
        }
      }
      LayerProperty() : LayerProperty(DEFAULT_LAYER_NUMBER) {}

      uint8_t getLayer() const { return layer; }
      bool setLayer(uint8_t new_layer) {
        if (new_layer > MAX_LAYER_NUMBER)
          return false;
        layer = new_layer;
        return true;
      }
  };

}

#endif // SILHOUETTE_LAYER_HXX
