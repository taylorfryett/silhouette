//
// Created by tfryett on 4/23/21.
//

#ifndef SILHOUETTE_DATATYPE_HXX
#define SILHOUETTE_DATATYPE_HXX

namespace sil {

  constexpr uint8_t MAX_DATATYPE_NUMBER = 63;
  constexpr uint8_t DEFAULT_DATATYPE = 0;

  class DatatypeProperty {
  private:
    uint8_t datatype;

  public:
    DatatypeProperty(uint8_t my_datatype) {
      if (!setDatatype(my_datatype)) {
        throw std::invalid_argument("Invalid datatype. "
                                    "Datatypes must be between 0 and " +
                                    std::to_string(MAX_DATATYPE_NUMBER) +
                                    ". Received: " + std::to_string(my_datatype) +
                                    ".");
      }
    }
    DatatypeProperty() : DatatypeProperty(DEFAULT_DATATYPE) {}

    uint8_t getDatatype() const { return datatype; }
    bool setDatatype(uint8_t new_datatype) {
      if (new_datatype > MAX_DATATYPE_NUMBER)
        return false;
      datatype = new_datatype;
      return true;
    }
  };

}

#endif // SILHOUETTE_DATATYPE_HXX
