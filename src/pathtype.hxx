//
// Created by tfryett on 4/24/21.
//

#ifndef SILHOUETTE_PATHTYPE_HXX
#define SILHOUETTE_PATHTYPE_HXX

namespace sil {
  const uint8_t MAX_PATHTYPE_NUMBER = 2;
  const uint8_t DEFAULT_PATHTYPE_NUMBER = 0;

  class PathtypeProperty {
      private:
      uint8_t pathtype;

      public:
      PathtypeProperty() {
        setPathtype(DEFAULT_PATHTYPE_NUMBER);
      }
      PathtypeProperty(uint8_t my_pathtype) {
        setPathtype(my_pathtype);
      }

      uint8_t getPathtype() const {
        return pathtype;
      }
      bool setPathtype(uint8_t new_pathtype) {
        if (new_pathtype > MAX_PATHTYPE_NUMBER)
          return false;
        pathtype = new_pathtype;
        return true;
      }
  };
}

#endif // SILHOUETTE_PATHTYPE_HXX
