// This file is a part of silhouette (2D Geometry suite)
//
// Copyright 2015 Taylor Fryett
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "path.hxx"

namespace sil {

  Path::Path(std::vector<CoordPnt> usrCoordPath, double usrPathWidth, 
	     int usrPathType, int usrLayer, int usrDataType) {
    setCoordPath(usrCoordPath);
    pathWidth = usrPathWidth;
    setPathtype(usrPathType); // check for validity and then set value
    setLayer(usrLayer);
    setDatatype(usrDataType);
  }

  Path::Path(std::vector<CoordPnt> usrCoordPath, double usrPathWidth, 
	     int usrPathType, int usrLayer) :
    Path::Path(usrCoordPath, usrPathWidth, usrPathType, usrLayer, 0) {}

  Path::Path(std::vector<CoordPnt> usrCoordPath, double usrPathWidth, 
	     int usrPathType) :
    Path::Path(usrCoordPath, usrPathWidth, usrPathType, 0) {}

  Path::Path(std::vector<CoordPnt> usrCoordPath, double usrPathWidth) :
    Path::Path(usrCoordPath, usrPathWidth, 0) {}

  Path::Path(std::vector<CoordPnt> usrCoordPath) :
    Path::Path(usrCoordPath, 0) { 
  }

  double Path::getPathWidth() const {
    return pathWidth;
  }

  void Path::setPathWidth(double newPathWidth) {
    pathWidth = newPathWidth;
  }

  std::vector<CoordPnt> Path::getCoordPath() const {
    return coordPath;
  }

  void Path::setCoordPath(std::vector<CoordPnt> newCoordPath) {
    if (newCoordPath.size() > 1 && newCoordPath.size() < 200) 
      coordPath = newCoordPath;
    else {
      std::stringstream errorMsg;
      errorMsg << "The number of coordinates in a Path must"
	       << " be in the range 2-199. User tried to set" 
	       << " a path with " << newCoordPath.size() << ".\n";
      throw std::invalid_argument(errorMsg.str());
    }
  }

  void Path::appendToCoordPath(CoordPnt nextCoordPnt) {
    if (coordPath.size() < 199) // can have up to 199 coordiantes
      coordPath.push_back(nextCoordPnt);
    else {
      std::stringstream errorMsg;
      errorMsg << "Path has its maximum number of coordinates already.\n";
      throw std::invalid_argument(errorMsg.str());
    }
  }


}
