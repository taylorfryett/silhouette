// This file is a part of silhouette (2D Geometry suite)
//
// Copyright 2015 Taylor Fryett
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#ifndef SILHOUETTE_PATH_HXX
#define SILHOUETTE_PATH_HXX

#include "coord.hxx"
#include <vector>
#include <stdexcept>
#include <sstream>
#include "layer.hxx"
#include "datatype.hxx"
#include "pathtype.hxx"

namespace sil {

  class Path : public LayerProperty, public DatatypeProperty, public PathtypeProperty {
  private:
    std::vector<CoordPnt> coordPath; //!< The points through which the path traverses.
    double pathWidth; //!< The width of the path.

  protected:
    
  public:
    //! Full constructor for the path class.
    Path(std::vector<CoordPnt> usrCoordPath, double usrPathWidth, 
	 int usrPathType, int usrLayer, int usrDataType);

    Path(std::vector<CoordPnt> usrCoordPath, double usrPathWidth, 
	 int usrPathType, int usrLayer);

    Path(std::vector<CoordPnt> usrCoordPath, double usrPathWidth, 
	 int usrPathType);

    Path(std::vector<CoordPnt> usrCoordPath, double usrPathWidth);

    Path(std::vector<CoordPnt> usrCoordPath);

    double getPathWidth() const;

    void setPathWidth(double newPathWidth);

    std::vector<CoordPnt> getCoordPath() const;

    void setCoordPath(std::vector<CoordPnt> newCoordPath);

    void appendToCoordPath(CoordPnt nextCoordPnt);

  };

}

#endif
