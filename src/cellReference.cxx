// This file is a part of silhouette (2D Geometry suite)
//
// Copyright 2015 Taylor Fryett
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "cellReference.hxx"
#include "cell.hxx"

namespace sil {

    CellReference::CellReference(Cell &referenceCell, int centerX,
                                 int centerY) : refCell(referenceCell),
                                                center(CoordPnt(centerX,
                                                                centerY)),
                                                magnification(1),
                                                rotation(0) {}

    CellReference::CellReference(Cell &referenceCell) :
            CellReference(referenceCell, 0, 0) {}

  CellReference::CellReference(Cell& referenceCell, const CoordPnt& centerPnt) :
          CellReference(referenceCell, centerPnt.x, centerPnt.y) {}


  std::vector<CoordPnt> CellReference::findVertices() {
    std::vector<CoordPnt> boundingVertex;
    std::vector<Polygon> polygonVec = refCell.getPolygonList();
    double totMax = std::numeric_limits<double>::max();
    double totMin = std::numeric_limits<double>::min();
    double minX = totMax;
    double minY = totMax;
    double maxX = totMin;
    double maxY = totMin;
    // find the minimum and maximum values of x and y coordinates
    for(std::vector<Polygon>::iterator polyIt = polygonVec.begin(); 
	polyIt != polygonVec.end(); ++polyIt) {
      std::vector<CoordPnt> vertexVec = polyIt->getVertices();
      for (std::vector<CoordPnt>::iterator vertIt = vertexVec.begin();
	   vertIt != vertexVec.end(); ++vertIt) {
	if (vertIt->x > maxX)
	  maxX = vertIt->x;
	if (vertIt->x < minX)
	  minX = vertIt->x;
	if (vertIt->y > maxY)
	  maxY = vertIt->y;
	if (vertIt->y < minY)
	  minY = vertIt->y;
      }
    }
    // check to make sure they actually changed into something
    if ((minX == maxX) && (minY == maxY))
      throw std::logic_error("Invalid polygons detected.");

    boundingVertex.emplace_back(minX, minY);
    boundingVertex.emplace_back(minX, maxY);
    boundingVertex.emplace_back(maxX, minY);
    boundingVertex.emplace_back(maxX, maxY);
    return boundingVertex;
  }

  double CellReference::getMagnification() const {
    return this->magnification;
  }

  void CellReference::setMagneification(double newMagnification) {
    this->magnification = newMagnification;
  }

  double CellReference::getRotation() const {
    return this->rotation;
  }

  void CellReference::setRotation(double newRotation) {
    this->rotation = newRotation;
  }

  CoordPnt CellReference::getCenter() const {
    return this->center;
  }

  void CellReference::setCenter(const CoordPnt& newCenter) {
    this->center = newCenter;
  }

  std::string CellReference::getCellname() const {
    return this->refCell.getCellname();
  }
}
